var net = require("net");
var JSONStream = require('JSONStream');

var serverHost = process.argv[2];
var serverPort = process.argv[3];
var botName = process.argv[4];
var botKey = process.argv[5];

console.log("I'm", botName, "and connect to", serverHost + ":" + serverPort);

client = net.connect(serverPort, serverHost, function() {
  return send({
    msgType: "join",
    data: {
      name: botName,
      key: botKey
    }
  });
});

function send(json) {
  client.write(JSON.stringify(json));
  return client.write('\n');
};


function safeToFloorIt(pieceIndex){
  //console.log(pieceIndex+" VS. "+trackData.length);
  //if(pieceIndex<trackData.length){

    if((trackData[pieceIndex+1]===true)&&(trackData[pieceIndex+2]===true)&&(trackData[pieceIndex+3]===true)){
      return 100;
    }
    if((trackData[pieceIndex+1]===true)&&(trackData[pieceIndex+2]!==true)){
      return 65;
    }
    
    if(pieceIndex===trackData.length-1){
      if((trackData[0]===true)&&(trackData[1]===true)&&(trackData[2]===true)){
        return 100;
      }
      console.log("second to last");
      console.log("1:"+trackData[pieceIndex]);

      console.log("2:"+trackData[0]);

      console.log("3:"+trackData[1]);
    
    }
    //}
/*
    if(pieceIndex===trackData.length-1){
      return 100;
    }
*/
    return false;
  

  
}

jsonStream = client.pipe(JSONStream.parse());


var memoizeDecreaseBy=0;
var angle = 0;
var lastSpeed = 0;
var topAngle = 0;
var trackData = [];
jsonStream.on('data', function(data) {

  //var info = JSON.parse(data);
  var maxThrottle = 0.653;
  var masterSpeed = maxThrottle*100;
  var increaseBy = 0;
  var maxAngle = 50;
  var EXP = 0.22;
  var decreaseBy = null;
  
  if(data.data){
    if(data.data[0]){
      //console.log(data.data[0].piecePosition);
      //console.log(safeToFloorIt(data.data[0].piecePosition.pieceIndex));
      angle=Math.abs(data.data[0].angle);

      if(angle<0.001){
        masterSpeed = maxThrottle*127;
      }
      if(angle<0.01){
        masterSpeed = maxThrottle*117;
      }  
      if(angle<0.1){
        masterSpeed = maxThrottle*107;
      }  
      //decreaseBy = angle;
      if(safeToFloorIt(data.data[0].piecePosition.pieceIndex)){
        masterSpeed = safeToFloorIt(data.data[0].piecePosition.pieceIndex);
      }
    }
  }

  masterSpeed /=100;
  
  if(lastSpeed!==masterSpeed){
    console.log("speed change:"+masterSpeed+" angle:"+angle+" topAngle:"+topAngle);
  }
  lastSpeed=masterSpeed;
  if(angle>topAngle){
    topAngle = angle;
  }
  if(data.msgType === 'gameInit') {
    console.log(data.data.race);
    for(var i=0;i<data.data.race.track.pieces.length;i++){
      console.log(data.data.race.track.pieces[i]);
      trackData.push(data.data.race.track.pieces[i].angle || true);
    }
    console.log(trackData);
    //console.log("Last angle:"+angle);
  }


  if(data.msgType === 'crash') {
    console.log("CRASH");
    //console.log("Last angle:"+angle);
  }
  if(data.msgType==='lapFinished'){
    console.log(data.data.lapTime.millis);
  }
  if (data.msgType === 'carPositions') {
    send({
      msgType: "throttle",
      //breaking point 0.656
      data: masterSpeed
    });
  } else {
    if (data.msgType === 'join') {
      console.log('Joined')
    } else if (data.msgType === 'gameStart') {
      console.log('Race started');
    } else if (data.msgType === 'gameEnd') {
      console.log('Race ended');
    } 

    send({
      msgType: "ping",
      data: {}
    });
  }
});

jsonStream.on('error', function() {
  return console.log("disconnected");
});
